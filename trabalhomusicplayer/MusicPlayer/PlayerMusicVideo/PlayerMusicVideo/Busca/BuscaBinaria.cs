﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Busca
{
    static class BuscaBinaria
    {
        public static int AlgBuscaBinaria(int valor, int[] arquivos, int posicaoInicio, int posicaoFim)
        {
            int meio = (posicaoInicio + posicaoFim) / 2;
            if (arquivos[meio] == valor)
                return meio;
            if (valor > arquivos[meio])
                return AlgBuscaBinaria(valor, arquivos, meio, posicaoFim);
            else
                return AlgBuscaBinaria(valor, arquivos, posicaoInicio, meio);
        }
    }
}
