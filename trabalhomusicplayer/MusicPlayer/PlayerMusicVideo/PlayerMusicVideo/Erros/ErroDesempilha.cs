﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    class ErroDesempilha : Exception
    {
        public ErroDesempilha(string erro) : base(erro) { }
    }
}
