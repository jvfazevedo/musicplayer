﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    class ErroFila : Exception
    {
        public ErroFila(string erro) : base(erro) { }
    }
}
