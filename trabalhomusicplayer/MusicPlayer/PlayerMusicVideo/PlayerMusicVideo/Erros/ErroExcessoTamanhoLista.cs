﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    class ErroExcessoTamanhoLista : Exception
    {
        public ErroExcessoTamanhoLista(string erro) : base(erro) { }
    }
}
