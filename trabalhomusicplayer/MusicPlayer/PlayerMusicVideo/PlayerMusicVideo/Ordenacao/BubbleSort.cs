﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Ordenacao
{
    static class BubbleSort
    {
        public static void OrdenacaoBubbleSorteInt(ref int[] arquivos, int n_elementos)
        {
            int aux = 0;
            for (int i = 0; i < n_elementos; i++)
            {
                for (int j = i; j < n_elementos; j++)
                {
                    if (arquivos[i] > arquivos[j])
                    {
                        aux = arquivos[i];
                        arquivos[i] = arquivos[j];
                        arquivos[j] = aux;
                    }
                }
            }
        }
        
    }
}
