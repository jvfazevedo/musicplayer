﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Ordenacao
{
    static class InsertionSort
    {
        public static void OrdenacaoInsertionSort(ref int[] arquivos)
        {
            int i, j, atual;
            for (i = 0; i < arquivos.Length; i++)
            {
                atual = arquivos[i];
                j = i;
                while ((j > 0) && (arquivos[j - 1] > atual))
                {
                    arquivos[j] = arquivos[j - 1];
                    j = j - 1;
                }
                arquivos[j] = atual;
            }
        }
    }
}
