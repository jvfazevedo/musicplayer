﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Classes
{
    public abstract class Midia
    {
        private int id;
        private string descricao;
        private string nome;

        #region Encapsulamento
        public string Nome
        {
            get { return nome; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Nome inválido");
                else
                    nome = value;
            }
        }
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
        public string Descricao
        {
            get
            {
                return descricao;
            }

            set
            {
                descricao = value;
            }
        }
        #endregion

        #region Métodos Abstratos
        public abstract void Incluir(Object arquivo);
        public abstract void Alterar(Object arquivo,string line);
        public abstract void Excluir(Object arquivo);
        //public abstract Midia Consultar(int id);//Verificar o método
        #endregion

    }
}
