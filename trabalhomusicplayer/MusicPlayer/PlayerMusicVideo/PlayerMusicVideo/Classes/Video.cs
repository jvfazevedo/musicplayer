﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PlayerMusicVideo.Classes
{
    class Video:Midia,ILocal, ICatalogo
    {
        private string arquivoMidia;
        private int anoLancamento;
        private bool possuiLegenda;
        private Enumeradores.FormatoEnumVideo formato;
        private Enumeradores.EnumIdiomasVideo idioma;

        #region Enumeradores
        public Enumeradores.FormatoEnumVideo Formato
        {
            get { return formato; }
            set { formato = value; }
        }
        public Enumeradores.EnumIdiomasVideo Idioma
        {
            get { return idioma; }
            set { value = idioma; }
        }
        #endregion

        #region Encapsulamento
        public bool PossuiLegenda
        {
            get { return possuiLegenda; }
            set
            { possuiLegenda = value; }
        }
        
        #endregion

        #region Métodos Midia
        public override void Incluir(object arquivo)
        {
            if (!File.Exists(@"..\..\videos"))
                Directory.CreateDirectory(@"..\..\videos");
            if (arquivo is Video)
            {
                string aux = (arquivo as Video).Id + "|" + (arquivo as Video).Nome + '|' + (arquivo as Video).Descricao + '|' + (arquivo as Video).idioma.ToString()+'|'+(arquivo as Video).PossuiLegenda
                    + '|' + (arquivo as Video).formato.ToString() + '|' + (arquivo as Video).AnoLancamento.ToString()+'|'+(arquivo as Video).ArquivoMidia + Environment.NewLine;
                File.AppendAllText(@"..\..\videos\dadosVideo.txt", aux);
            }
        }   
        public override void Alterar(object arquivo,string line)
        {
            if (!File.Exists("../videos"))
                throw new Exception("Caminho não encontrado");
        }
        public override void Excluir(object arquivo)
        {
            if (!File.Exists("../videos"))
                throw new Exception("Caminho não encontrado");
        }
        #endregion

        #region ILocal

        public string ArquivoMidia
        {
            get
            {
                return arquivoMidia;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Erro arquivo");
                else
                    arquivoMidia = value;
            }
        }


        public bool ValidaCaminho(string caminho)
        {
            if (System.IO.File.Exists(caminho))
                return true;
            else
                return false;
        }
        #endregion

        #region ICatalogo
        public int AnoLancamento
        {
            get
            {
                return anoLancamento;
            }

            set
            {
                if (anoLancamento < 0 )
                    throw new Exception("Erro Lançamento");
                else
                    anoLancamento = value;
            }
        }
        #endregion
    }
}
