﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Classes
{
    class Playlist
    {

        List<Musica> musicas = new List<Musica>();
        List<Musica> musicasPlayListSelecionada;
        List<string> removendoPlayList;


        Musica apoio;

        public List<Musica> Musicas
        {
            get
            {
                return musicas;
            }

            set
            {
                musicas = value;
            }


        }
        public List<Musica> MusicasPlayListSelecionada
        {
            get
            {
                return musicasPlayListSelecionada;
            }

            set
            {
                musicasPlayListSelecionada = value;
            }


        }
        /// <summary>
        /// Adiciona as musicas para uma lista do C#
        /// </summary>
        public void Add()
        {
            string[] quebrandoLinhas = File.ReadAllLines("dadosMusica.txt");
            for (int i = 0; i < quebrandoLinhas.Length; i++)
            {
                apoio = new Musica();
                string[] quebrandoPipe = quebrandoLinhas[i].Split('|');
                apoio.Id = Convert.ToInt32(quebrandoPipe[0]);
                apoio.Nome = quebrandoPipe[1];
                apoio.Descricao = quebrandoPipe[2];
                apoio.Volume = Convert.ToInt32(quebrandoPipe[3]);
                Enumeradores.FormatoEnumMusica form = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), quebrandoPipe[4].ToString());
                apoio.Formato = form;
                apoio.Duracao = double.Parse(quebrandoPipe[5]);
                apoio.ArquivoMidia = quebrandoPipe[6];
                Musicas.Add(apoio);

            }
        }
        /// <summary>
        /// cria um txt da playLnist com o nome
        /// </summary>
        /// <param name="nomePlayList"></param>
        /// <param name="addMusica"></param>
        public void CriarPlayList(string nomePlayList, List<Musica> addMusica)
        {

            if (File.Exists($"{nomePlayList}.txt"))
                throw new Exception("Ja existe uma play list com esse nome");

            for (int x = 0; x < addMusica.Count; x++)
            {

                string aux = (addMusica[x] as Musica).Id + "|" + (addMusica[x] as Musica).Nome + '|' + (addMusica[x] as Musica).Descricao + '|' + (addMusica[x] as Musica).Volume
                    + "|" + (addMusica[x] as Musica).Formato + '|' + (addMusica[x] as Musica).Duracao + '|' + (addMusica[x] as Musica).ArquivoMidia + Environment.NewLine;
                File.AppendAllText($"{nomePlayList}.txt", aux);
            }
            NomeDaPlayList(nomePlayList);


        }
        /// <summary>
        /// adiciona o nome de uma play list criada ao txt nomeDasPlayList
        /// </summary>
        /// <param name="nomePlayList"></param>
        public void NomeDaPlayList(string nomePlayList)
        {

            if (!File.Exists("nomeDasPlayList.txt"))
            {
                nomePlayList = nomePlayList + Environment.NewLine;
                File.AppendAllText("NomeDasPlayList.txt", nomePlayList);
            }
            else
            {
                string[] nomeList = File.ReadAllLines("nomeDasPlayList.txt");
                bool quebugChato = true;
                for (int i = 0; i < nomeList.Length; i++)
                {
                    if (nomeList[i] == nomePlayList)
                        quebugChato = false;
                }
                if (quebugChato == true)
                {
                    nomePlayList = nomePlayList + Environment.NewLine;
                    File.AppendAllText("NomeDasPlayList.txt", nomePlayList);
                }
            }


        }
        /// <summary>
        /// adiciona em uma lista do C# as musicas de uma derminada play list
        /// </summary>
        /// <param name="nomePlayList"></param>
        public void MusicasDaPlayList(string nomePlayList)
        {

            if (nomePlayList.Length == 0)
                throw new Exception("Selecione uma play list para ser alterada");
            string[] quebrandoLinhas = File.ReadAllLines($"{nomePlayList}.txt");
            if (quebrandoLinhas.Length != 0)
            {


                for (int i = 0; i < quebrandoLinhas.Length; i++)
                {
                    apoio = new Musica();
                    string[] quebrandoPipe = quebrandoLinhas[i].Split('|');
                    apoio.Id = Convert.ToInt32(quebrandoPipe[0]);
                    apoio.Nome = quebrandoPipe[1];
                    apoio.Descricao = quebrandoPipe[2];
                    apoio.Volume = Convert.ToInt32(quebrandoPipe[3]);
                    Enumeradores.FormatoEnumMusica form = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), quebrandoPipe[4].ToString());
                    apoio.Formato = form;
                    apoio.Duracao = double.Parse(quebrandoPipe[5]);
                    apoio.ArquivoMidia = quebrandoPipe[6];
                    musicasPlayListSelecionada.Add(apoio);

                }
            }

        }
        public void AlterarPlayList(string nomePlayList, List<Musica> lista)
        {

            if (File.Exists($"{nomePlayList}.txt"))
                File.Delete($"{nomePlayList}.txt");
            CriarPlayList(nomePlayList, lista);

        }
        public void RemoverPlayList(string nomePlayList)
        {
            File.Delete($"{nomePlayList}.txt");
            removendoPlayList = new List<string>();
            string[] quebrandoLinhas = File.ReadAllLines("nomeDasPlayList.txt");
            for (int i = 0; i < quebrandoLinhas.Length; i++)
            {
                if (quebrandoLinhas[i] != nomePlayList)
                    removendoPlayList.Add(quebrandoLinhas[i]);
            }
            File.Delete("nomeDasPlayList.txt");
            for (int w = 0; w < removendoPlayList.Count; w++)
                File.AppendAllText("nomeDasPlayList.txt", removendoPlayList[w] + Environment.NewLine);

        }
        List<Musica> reproduzirPlayList;
        public List<Musica> ReproduzirPlayList
        {
            get
            {
                return reproduzirPlayList;
            }
            set
            {
                reproduzirPlayList = value;
            }
        }
        public void rodarPlayList(string nomePlayList)
        {
            reproduzirPlayList = new List<Musica>();
            if (!File.Exists($"nomePlayList.txt"))
                throw new Exception("Essa PlayList não possui musica");
            string[] quebraLinhas = File.ReadAllLines($"{nomePlayList}.txt");
            if (quebraLinhas.Length == 0)
                throw new Exception("Essa PlayList não possui musica");


            for (int i = 0; i < quebraLinhas.Length; i++)
            {
                Musica musica = new Musica();
                string[] quebrandoPipe = quebraLinhas[i].Split('|');
                musica.Id = Convert.ToInt32(quebrandoPipe[0]);
                musica.Nome = quebrandoPipe[1];
                musica.Descricao = quebrandoPipe[2];
                apoio.Volume = Convert.ToInt32(quebrandoPipe[3]);
                Enumeradores.FormatoEnumMusica form = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), quebrandoPipe[4].ToString());
                musica.Formato = form;
                musica.Duracao = double.Parse(quebrandoPipe[5]);
                musica.ArquivoMidia = quebrandoPipe[6];
                reproduzirPlayList.Add(apoio);

            }

        }
    }
}
