﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Classes
{
    public class AlbumMusical:Midia, ICatalogo
    {
        
        private string artista;
        private int anoLancamento;
        private string capaAlbum;

        public Lista lista = new Lista();
        public List<Musica> listaC = new List<Musica>();

        #region Encapsulamento

        public int AnoLancamento
        {
            get
            {
                return anoLancamento;
            }

            set
            {
                if (anoLancamento < 0)
                    throw new Exception("Erro ano lançamento");
                else
                    anoLancamento = value;
            }
        }
        public string Artista
        {
            get
            {
                return artista;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Erro ao validar artista");
                else
                    artista = value;
            }
        }
        public string CapaAlbum
        {
            get { return capaAlbum;}
            set { capaAlbum = value;}
        }
        #endregion
        #region Métodos Hernaça Midia

        
        public override void Incluir(Object arquivo)
        {
            if (!File.Exists(@"..\..\albuns"))
            {
                Directory.CreateDirectory(@"..\..\albuns");
                
            }
            
            string aux = (arquivo as AlbumMusical).Nome + "|" + (arquivo as AlbumMusical).Id + "|" + (arquivo as AlbumMusical).Artista + "|" + (arquivo as AlbumMusical).AnoLancamento.ToString() + "|" +
                lista.ListarIterativo().ToString() + Environment.NewLine;
            
            File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory.ToString()+@"\dadosAlbum.txt", aux);
            if(!string.IsNullOrEmpty((arquivo as AlbumMusical).CapaAlbum))
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory.ToString() + @"\dadosCapaAlbum.txt", (arquivo as Classes.AlbumMusical).CapaAlbum+Environment.NewLine);
        }
        public override void Alterar(Object arquivo,string line)
        {
            if (Directory.Exists(@"..\..\albuns")==false)
                throw new Exception("Erro alterar");
            else
            {
                string[] vetor = File.ReadAllLines("dadosAlbum.txt");
                for(int i = 0; i < vetor.Length; i++)
                {
                    if(line==vetor[i])
                    {
                        string aux = (arquivo as AlbumMusical).Nome + "|" + (arquivo as AlbumMusical).Id + "|" + (arquivo as AlbumMusical).Artista + "|" + (arquivo as AlbumMusical).AnoLancamento.ToString() + "|" +
                   lista.ListarIterativo().ToString();
                        aux = aux.Replace("\r\n", "|");
                        vetor[i]=aux;
                        break;
                    }
                }
                File.WriteAllLines("dadosAlbum.txt", vetor);
            }
        }
        public override void Excluir(Object arquivo)
        {
            
        }
        #endregion

        //Lista com as músicas -> Utilizar a lista
    }
}
