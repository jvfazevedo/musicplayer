﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Classes
{
    public class Foto : Midia, ILocal, ICatalogo
    {
        private string local;
        private double megaPixels;
        private int tempoExibir;
        private int anoLancamento;
        private string arquivoMidia;

        #region Encapsulamento
        public string Local
        {
            get { return local; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Local vazio");
                else
                    local = value;
            }
        }
        public double MegaPixels
        {
            get { return megaPixels; }
            set
            {
                if (value < 0)
                    throw new Exception("Valor incorreto");
                else
                    megaPixels = value;
            }
        }
        public int TempoExibir
        {
            get { return tempoExibir; }
            set
            {
                if (value > 0)
                    tempoExibir = value;
            }
        }
        #endregion

        #region Métodos Midia
        public override void Incluir(object arquivo)
        {
            if (!File.Exists(@"..\..\foto"))
                Directory.CreateDirectory(@"..\..\foto");
            if (arquivo is Foto)
            {
                string aux = (arquivo as Foto).Id + "|" + (arquivo as Foto).Nome + '|' + (arquivo as Foto).Descricao + '|' + (arquivo as Foto).Local +"|"+
                (arquivo as Foto).MegaPixels.ToString()  + "|" + (arquivo as Foto).TempoExibir.ToString() + "|" + (arquivo as Foto).AnoLancamento+ "|" + (arquivo as Foto).ArquivoMidia + Environment.NewLine;
                File.AppendAllText(@"..\..\foto\dadosFoto.txt", aux);
            }

        }
        public override void Alterar(object arquivo,string line)
        {
            if (!System.IO.File.Exists("../fotos"))
                throw new Exception("../fotos");
        }
        public override void Excluir(object arquivo)
        {
            if (!System.IO.File.Exists(@"..\..\foto"))
                throw new Exception(@"..\..\foto");
        }
        #endregion

        #region Encapsulamento ICatalogo
        public int AnoLancamento
        {
            get
            {
                return anoLancamento;
            }

            set
            {
                if (anoLancamento < 0)
                    throw new Exception("Erro Lançamento");
                else
                    anoLancamento = value;
            }
        }
        #endregion

        #region Encapsulamento e metodo ILocal

        public string ArquivoMidia
        {
            get
            {
                return arquivoMidia;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Erro arquivo");
                else
                    arquivoMidia = value;
            }
        }


        public bool ValidaCaminho(string caminho)
        {
            if (System.IO.File.Exists(caminho))
                return true;
            else
                return false;
        }
        #endregion
    }
}
