﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Classes
{
    class DataObject
    {
        public string Nome{ get; set; }
        public string Album { get; set; }
        public string Artista { get; set; }
        public double Duracao { get; set; }
        public string Descricao { get; set; }
    }
}
