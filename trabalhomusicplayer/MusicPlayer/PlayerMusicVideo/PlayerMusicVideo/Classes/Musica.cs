﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Classes
{
    public class Musica : Midia, ILocal
    {
        
        private double duracao;
        private int volume;
        private string arquivoMidia;


        #region Enumerador
        private Enumeradores.FormatoEnumMusica formato;
        #endregion

        #region Métodos Hernaça Midia

        public override void Incluir(Object arquivo)
        {
            if (!File.Exists(@"..\..\musicas"))
                Directory.CreateDirectory(@"..\..\musicas");
            if (arquivo is Musica)
            {
                string aux = (arquivo as Musica).Id + "|" + (arquivo as Musica).Nome + '|' + (arquivo as Musica).Descricao + '|' + (arquivo as Musica).Volume + "|" +
                  (arquivo as Musica).Formato.ToString()  + '|' + (arquivo as Musica).Duracao + '|' + (arquivo as Musica).ArquivoMidia+Environment.NewLine;
                File.AppendAllText("dadosMusica.txt", aux);
            }
            
        }
        public override void Alterar(Object arquivo,string line)
        {
            if (!File.Exists(@"..\..\musicas"))
                throw new Exception("Caminho não encontrado");

        }
        public override void Excluir(Object arquivo)
        {
            if (!File.Exists(@"..\..\musicas"))
                throw new Exception("Caminho não encontrado");
            throw new NotImplementedException();
        }
        #endregion




        #region Encapsulamento
        public Enumeradores.FormatoEnumMusica Formato
        {
            get { return formato; }
            set
            {
                formato = value;
            }
        }
        public double Duracao
        {
            get { return duracao; }
            set
            {
                if (value > 0)
                    duracao = value;
                else
                    throw new Exception("Duracao incorreta");
            }
        }
        public int Volume
        {
            get { return volume; }
            set
            {
                if (value > 0)
                    volume = value;
                else
                    throw new Exception("Volume incorreto");
            }
        }

        #endregion

        

        #region Métodos Interface ILocal

        public string ArquivoMidia
        {
            get
            {
                return arquivoMidia;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Erro arquivo");
                else
                    arquivoMidia = value;
            }
        }

        public bool ValidaCaminho(string caminho)
        {
            if (System.IO.File.Exists(caminho))
                return true;
            else
                return false;
        }
        #endregion
    }
}
