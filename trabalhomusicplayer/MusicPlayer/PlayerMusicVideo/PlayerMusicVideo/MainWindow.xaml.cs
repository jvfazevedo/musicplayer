﻿using PlayerMusicVideo.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Threading;
using System.Collections.ObjectModel;

namespace PlayerMusicVideo
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Remove todos os itens do ComboBox, mesma função que o refresh();
        /// </summary>
        /// <param name="cb"></param>
        public void RemoverItemsComboBox(ComboBox cb)
        {
            int x = cb.Items.Count;
            for (int i = 0; i < x; i++)
            {
                cb.Items.Remove(cb.Items[0]);
            }

        }
        /// <summary>
        /// remove todos os itens do listbox mesma função, refresh();
        /// </summary>
        /// <param name="lb"></param>
        public void RemoverListBox(ListBox lb)
        {
            int x = lb.Items.Count;
            for (int i = 0; i < x; i++)
            {
                lb.Items.Remove(lb.Items[0]);
            }
        }
        /// <summary>
        /// preenche os dados inicias da tela
        /// </summary>
        public void PreencheTudo()
        {


            if (File.Exists("dadosMusica.txt"))
            {
                help.Add();

                for (int i = 0; i < help.Musicas.Count; i++)
                {
                    lbMusica.Items.Add(help.Musicas[i].Nome);

                }
                q++;
            }
            else
            {
                gridConfigPlaylist.Visibility = Visibility.Hidden;
                MessageBox.Show("não existe nenhuma musicas cadastrada");
                q = 0;
            }

        }
        /// <summary>
        /// Redefinição da variavel
        /// </summary>
        /// <param name="q"></param>
        public void qPlaylist(ref int q)
        {
            q = 0;

        }
        /// <summary>
        /// Reproduzir em lista
        /// </summary>
        /// <param name="caminho"></param>
        public void ReproduzirLista(string caminho)
        {
            Reproduzir(caminho, ref playing);
        }
        /// <summary>
        /// Carregamento da lista
        /// </summary>
        public void CarregarLista()
        {
            foreach (var l in musicaPlaylist)
            {
                listaC.InserirNoFim(l);
            }
            //Reproduzir((listaC.RemoverDaPosicao(0) as Musica).ArquivoMidia, ref playing);
        }
        /// <summary>
        /// Carregamento da pilha
        /// </summary>
        public void CarregarPilha()
        {
            
            {
                foreach (var l in musicaPlaylist)
                {
                    pilhaC.Empilhar(l);
                }
            }
        }
        /// <summary>
        /// Carregamento da fila
        /// </summary>
        public void CarregarFila()
        {
            
            foreach (var l in musicaPlaylist)
            {
                filaC.AdicionarNaFila(l);
            }
        }

        //Variavel para validação
        public bool test = true;
        //Variável da playlist
        public int q = 0;
        //Marcadores de 
        public int marcadorP = 0;

        public int marcadorF = 0;
        //Playlist auxiliar
        Playlist help;
        //Instância da lista
        public Lista listaC = new Lista();

        public bool controleAlterar = true;

        public int passo = 0; //Variável, marcar o passo do andamento

        public int marcadorPlaylist = 0;
        public int marcadorFoto = 0;

        public int countFoto = 0;

        public int marcador = 0; //Marca a música que está tocando


        //Variável da validação
        public bool playing = false;
        ///Método para atualizar playlist
        public void AtualizarPlayList()
        {
            if (gridConfigPlaylist.Visibility == Visibility.Hidden)
            {
                if (File.Exists("nomeDasPlayList.txt"))
                {
                    lsBarraLateral.Items.Clear();
                    string[] readerNomesPlaylist = File.ReadAllLines("nomeDasPlayList.txt");
                    foreach (string l in readerNomesPlaylist)
                    {
                        lsBarraLateral.Items.Add(l);
                    }
                }
            }
        }
        /// <summary>
        /// Método para preencher com as fotos
        /// </summary>
        public void PreencherFoto()
        {
            var caminho = ImagemUri(listaFoto[marcadorFoto].ArquivoMidia.ToString());
            imFotoExibir.Source = caminho;
            txtNomeFotoExibir.Text = listaFoto[marcadorFoto].Nome;
            txtLocalFotoExibir.Text = listaFoto[marcadorFoto].Local;
            txtDescFotoExibir.Text = listaFoto[marcadorFoto].Descricao;
        }
        /// <summary>
        /// Preenchendo btn mdo album
        /// </summary>
        /// <param name="image"></param>
        /// <param name="labelNome"></param>
        /// <param name="nomeAlbum"></param>
        public void PreencherBtnAlbum(Image image, Label labelNome, Label nomeAlbum)
        {
            btnAlbum.Source = image.Source;
            lbNomeMusicaOuvindo.Content = labelNome.Content;
            lbNomeAlbum.Content = nomeAlbum.Content;
        }
        //Vetor para os caminhos da músicas
        public string[] caminhosMusica;
        //Instâncias da pilha e fila
        public Pilha pilhaC = new Pilha();
        public Fila filaC = new Fila();
        /// <summary>
        /// Lista para playlist
        /// </summary>
        public List<Musica> musicaPlaylist = new List<Musica>();
        public List<Foto> listaFoto = new List<Foto>();

        /// <summary>
        /// Listas dos albuns com as músicas
        /// </summary>
        public List<Midia> listaMusicasAlbum = new List<Midia>();
        /// <summary>
        /// Lista de todas as músicas
        /// </summary>
        public List<Musica> listaMusicas = new List<Musica>();

        List<Video> listaVideos = new List<Video>();
        /// <summary>
        /// Método para montar a imagem em BitmapImage
        /// </summary>
        /// <param name="caminho"></param>
        /// <returns></returns>
        public BitmapImage ImagemUri(string caminho)
        {
            BitmapImage bi3 = new BitmapImage();
            bi3.BeginInit();
            bi3.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory.ToString() + caminho);
            bi3.EndInit();
            return bi3;
        }
        /// <summary>
        /// Método para reproduzir no player
        /// </summary>
        /// <param name="caminho"></param>
        /// <param name="playing"></param>
        public void Reproduzir(string caminho, ref bool playing)
        {
            mePlayer.LoadedBehavior = MediaState.Manual;
            mePlayer.UnloadedBehavior = MediaState.Manual;
            mePlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory.ToString() + caminho);
            mePlayer.Play();
            playing = true;
        }
        /// <summary>
        /// Método para limpar os pictures box
        /// </summary>
        public void LimparPictureBox()
        {


            //Zerando pbox
            image.Source = null;
            image1.Source = null;
            image2.Source = null;
            image3.Source = null;
            image4.Source = null;
            image5.Source = null;

            //Zerando labels
            label1.Content = "";
            label2.Content = "";
            label3.Content = "";
            label4.Content = "";
            label5.Content = "";
            label6.Content = "";
            label7.Content = "";
            label8.Content = "";
            label9.Content = "";
            label10.Content = "";
            label11.Content = "";
            lbId1.Content = "";
            lbId2.Content = "";
            lbId3.Content = "";
            lbId4.Content = "";
            lbId5.Content = "";
            lbId6.Content = "";
            label12.Content = "";



        }
        /// <summary>
        /// Marcador na lista
        /// </summary>
        public int marcadorListaC = 0;
        /// <summary>
        /// Carregando as pictures box
        /// </summary>
        /// <param name="lista"></param>
        /// <param name="passo"></param>
        public void CarregandoPictureBox(List<Midia> lista, int passo)
        {
            //LimparPictureBox();
            int count = 0;
            // caminhosMusica = new string[listaMusicas.Count];
            int aux = passo;
            var imagem = ImagemUri((lista[0] as AlbumMusical).CapaAlbum);


            if (count <= 6) //pb1
            {
                if (passo < listaMusicas.Count)
                {
                    for (int a = 0; a < lista.Count; a++)
                    {
                        if ((lista[a] as AlbumMusical).lista.Pesquisa((listaMusicas[passo] as Object)))
                        {
                            label9.Content = listaMusicas[passo].Nome;
                            label10.Content = (lista[a] as AlbumMusical).Nome;
                            lbId1.Content = listaMusicas[passo].ArquivoMidia;
                            imagem = ImagemUri((lista[a] as AlbumMusical).CapaAlbum);
                            // caminhosMusica[count] = lbId1.Content.ToString();
                        }
                    }
                    image4.Source = imagem;
                    count++;
                    passo++;
                }
            }


            if (count <= 6) //pb2
            {
                if (passo < listaMusicas.Count)
                {
                    for (int a = 0; a < lista.Count; a++)
                    {
                        if ((lista[a] as AlbumMusical).lista.Pesquisa(listaMusicas[passo]))
                        {
                            label1.Content = listaMusicas[passo].Nome;
                            label2.Content = (lista[a] as AlbumMusical).Nome;
                            lbId2.Content = listaMusicas[passo].ArquivoMidia;
                            imagem = ImagemUri((lista[a] as AlbumMusical).CapaAlbum);
                        }
                    }
                    image.Source = imagem;
                    count++;
                    passo++;
                }
            }


            if (count <= 6) //pb3
            {
                if (passo < listaMusicas.Count)
                {
                    for (int a = 0; a < lista.Count; a++)
                    {
                        if ((lista[a] as AlbumMusical).lista.Pesquisa(listaMusicas[passo]))
                        {
                            label3.Content = listaMusicas[passo].Nome;
                            label4.Content = (lista[a] as AlbumMusical).Nome;
                            lbId3.Content = listaMusicas[passo].ArquivoMidia;
                            imagem = ImagemUri((lista[a] as AlbumMusical).CapaAlbum);
                        }
                    }
                    image1.Source = imagem;
                    count++;
                    passo++;
                }
            }


            if (count <= 6) //pb4
            {
                if (passo < listaMusicas.Count)
                {
                    for (int a = 0; a < lista.Count; a++)
                    {
                        if ((lista[a] as AlbumMusical).lista.Pesquisa(listaMusicas[passo]))
                        {
                            label5.Content = listaMusicas[passo].Nome;
                            label6.Content = (lista[a] as AlbumMusical).Nome;
                            lbId4.Content = listaMusicas[passo].ArquivoMidia;
                            imagem = ImagemUri((lista[a] as AlbumMusical).CapaAlbum);
                        }
                    }
                    image2.Source = imagem;
                    count++;
                    passo++;
                }
            }


            if (count <= 6) //pb5
            {
                if (passo < listaMusicas.Count)
                {
                    for (int a = 0; a < lista.Count; a++)
                    {
                        if ((lista[a] as AlbumMusical).lista.Pesquisa(listaMusicas[passo]))
                        {
                            label7.Content = listaMusicas[passo].Nome;
                            label8.Content = (lista[a] as AlbumMusical).Nome;
                            lbId5.Content = listaMusicas[passo].ArquivoMidia;
                            imagem = ImagemUri((lista[a] as AlbumMusical).CapaAlbum);
                        }
                    }

                    image3.Source = imagem;
                    count++;
                    passo++;
                }
            }


            if (count <= 6) //pb6
            {
                if (passo < listaMusicas.Count)
                {
                    for (int a = 0; a < lista.Count; a++)
                    {
                        if ((lista[a] as AlbumMusical).lista.Pesquisa(listaMusicas[passo]))
                        {
                            label11.Content = listaMusicas[passo].Nome;
                            label12.Content = (lista[a] as AlbumMusical).Nome;
                            lbId6.Content = listaMusicas[passo].ArquivoMidia;
                            imagem = ImagemUri((lista[a] as AlbumMusical).CapaAlbum);
                        }
                    }
                    image5.Source = imagem;
                    passo++;
                    count++;
                }

            }



        } //Pb das músicas
        /// <summary>
        /// Metodo para conversao das escalas
        /// </summary>
        /// <param name="valormin1"></param>
        /// <param name="valormin2"></param>
        /// <param name="valormax1"></param>
        /// <param name="valormax2"></param>
        /// <param name="intermediario2"></param>
        /// <returns></returns>
        public static double ConversorDeEscala(double valormin1, double valormin2, double valormax1, double valormax2, double intermediario2)
        {
            double intermediario1 = -((valormax2 - intermediario2) / valormax2 - valormin2) * (valormax1 - valormin1) + valormax1;
            return intermediario1;
        }

        public MainWindow()
        {

        }
        /// <summary>
        /// Evento do timer do projeto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void timer_Tick(object sender, EventArgs e)
        {
            if (gridFotos.Visibility == Visibility.Visible)
            {
                countFoto++;
                if (countFoto == listaFoto[marcadorFoto].TempoExibir)
                {
                    if ((marcadorFoto + 1) < listaFoto.Count)
                    {
                        marcadorFoto++;
                        countFoto = 0;
                        PreencherFoto();
                    }
                    else
                    {
                        marcadorFoto = 0;
                        countFoto = 0;
                        PreencherFoto();
                    }
                }


            }

            if (playing == true)
            {

                pbMediaVideo.Minimum = 0;
                pbMediaVideo.Maximum = 100;
                var b = 0;
                var d = 0;
                if (mePlayer.NaturalDuration.HasTimeSpan)
                {
                    b = mePlayer.NaturalDuration.TimeSpan.Minutes;
                    d = mePlayer.NaturalDuration.TimeSpan.Seconds;
                    string formatoHoraMax = Convert.ToString(b) + ":" + Convert.ToString(d);
                    lbTimerMax.Content = formatoHoraMax;
                    double a = ConversorDeEscala(0, 0, 100, mePlayer.NaturalDuration.TimeSpan.TotalSeconds, mePlayer.Position.TotalSeconds);
                    pbMediaVideo.Value = Convert.ToInt32(a);
                    lbTimerMin.Content = Convert.ToString(mePlayer.Position.Minutes) + ":" + Convert.ToString(mePlayer.Position.Seconds);

                }

            }
            //lbTimerMin.Content = String.Format("{0:t}", mePlayer.Position.Minutes) + ":" + Convert.ToString(mePlayer.Position.Seconds);

        }
        /// <summary>
        /// Evento para quando entra em foco no txtnome 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBuscar_GotFocus(object sender, RoutedEventArgs e)
        {
            txtBuscar.Text = "";
        }
        /// <summary>
        /// Evento para perder o foco
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBuscar_LostFocus(object sender, RoutedEventArgs e)
        {
            txtBuscar.Text = "Buscar";
            
        }
        /// <summary>
        /// evento para configurações das playlists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbConfigPlayList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
            gridFotos.Visibility = Visibility.Hidden;
            gridPrincipalMedia.Visibility = Visibility.Hidden;
            gridVideoExibir.Visibility = Visibility.Hidden;
            gridMusicas2.Visibility = Visibility.Hidden;
            gridConfigPlaylist.Visibility = Visibility.Visible;
            cbPlayList.Visibility = Visibility.Hidden;
            btnAlterar.Visibility = Visibility.Hidden;
            btnRemovePlayList.Visibility = Visibility.Hidden;
            help = new Playlist();
            //Mostrar o grid das configurações
            test = true;
            //Quando aparece o grid
            try
            {


                if (q == 0)
                {
                    PreencheTudo();

                }
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message);
            }

        }
        /// <summary>
        /// eventos para quando entrarem 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCriar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }
        /// <summary>
        /// Evento para a função remover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemoveMusicaPlaylist_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnAdd_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ckbAlterarPlayList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnAlterar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void lbMinhasMusicas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<Classes.DataObject> list = new List<Classes.DataObject>();

            string[] musicas = File.ReadAllLines(@"dadosMusica.txt");
            for (int i = 0; i < musicas.Length; i++)
            {
                string[] split = musicas[i].Split('|');
                list.Add(new Classes.DataObject { Nome = split[1], Duracao = double.Parse(split[0]), Descricao = split[2] });
            }
            dataGrid.Visibility = Visibility.Visible;
            dataGrid.Items.Clear();
            this.dataGrid.ItemsSource = list;


        }

        private void lbCadastrar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CadastrarScreen telaCadastro = new CadastrarScreen();
            telaCadastro.ShowDialog();
            listaMusicas = new List<Musica>();
            listaMusicasAlbum = new List<Midia>();
            PrincipalScreen_Loaded(sender, e);



        }
        /// <summary>
        /// Método para carregar os arquivos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrincipalScreen_Loaded(object sender, RoutedEventArgs e)
        {
            #region Timer   
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
            #endregion

            rdLista.IsChecked = true;
            #region Carregamento Playlist 
            if (File.Exists("nomeDasPlayList"))
            {
                string[] carregamento = File.ReadAllLines("nomeDasPlayList.txt");
                for (int i = 0; i < carregamento.Length; i++)
                {
                    string[] playlist = carregamento[i].Split('|');
                    lsBarraLateral.Items.Add(playlist[1]);
                }
            }
            #endregion // Falta terminar
            CarregarLista();//Colocar nos outros
            AtualizarPlayList();
            try
            {
                #region Carregamento das músicas com album em uma lista

                if (File.Exists("dadosAlbum.txt"))
                {
                    string[] readerAlbum = File.ReadAllLines("dadosAlbum.txt");
                    foreach (string l in readerAlbum)
                    {
                        string auxAlbum = l;
                        AlbumMusical albumAux = new AlbumMusical();
                        albumAux.Nome = auxAlbum.Substring(0, auxAlbum.IndexOf('|'));
                        auxAlbum = auxAlbum.Remove(0, auxAlbum.IndexOf('|') + 1);
                        albumAux.Id = int.Parse(auxAlbum.Substring(0, auxAlbum.IndexOf('|')));
                        auxAlbum = auxAlbum.Remove(0, auxAlbum.IndexOf('|') + 1);
                        albumAux.Artista = auxAlbum.Substring(0, auxAlbum.IndexOf('|'));
                        auxAlbum = auxAlbum.Remove(0, auxAlbum.IndexOf('|') + 1);
                        albumAux.AnoLancamento = int.Parse(auxAlbum.Substring(0, auxAlbum.IndexOf('|')));
                        auxAlbum = auxAlbum.Remove(0, auxAlbum.IndexOf('|') + 1);

                        do
                        {
                            if (File.Exists("dadosMusica.txt") && File.Exists("dadosAlbum.txt"))
                            {
                                string[] readerMusic = File.ReadAllLines("dadosMusica.txt");

                                foreach (string j in readerMusic)
                                {
                                    if (auxAlbum.IndexOf('|') != -1)
                                    {
                                        if (j.Substring(0, j.IndexOf('|')) == auxAlbum.Substring(0, auxAlbum.IndexOf('|')))
                                        {
                                            Musica musica = new Musica();
                                            musica.Id = int.Parse(j.Substring(0, j.IndexOf('|')));
                                            string auxMusica = j.Remove(0, j.IndexOf('|') + 1);
                                            musica.Nome = auxMusica.Substring(0, auxMusica.IndexOf('|'));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.Descricao = auxMusica.Substring(0, auxMusica.IndexOf('|'));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.Volume = int.Parse(auxMusica.Substring(0, auxMusica.IndexOf('|')));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.Formato = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), auxMusica.Substring(0, auxMusica.IndexOf('|')));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.Duracao = double.Parse(auxMusica.Substring(0, auxMusica.IndexOf('|')));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.ArquivoMidia = auxMusica;
                                            albumAux.lista.InserirNoFim(musica);
                                            auxAlbum = auxAlbum.Remove(0, auxAlbum.IndexOf('|') + 1);

                                        }
                                    }
                                    else
                                    {
                                        if (j.Substring(0, j.IndexOf('|')) == auxAlbum)
                                        {
                                            Musica musica = new Musica();
                                            musica.Id = int.Parse(j.Substring(0, j.IndexOf('|')));
                                            string auxMusica = j.Remove(0, j.IndexOf('|') + 1);
                                            musica.Nome = auxMusica.Substring(0, auxMusica.IndexOf('|'));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.Descricao = auxMusica.Substring(0, auxMusica.IndexOf('|'));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.Volume = int.Parse(auxMusica.Substring(0, auxMusica.IndexOf('|')));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.Formato = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), auxMusica.Substring(0, auxMusica.IndexOf('|')));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.Duracao = double.Parse(auxMusica.Substring(0, auxMusica.IndexOf('|')));
                                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                                            musica.ArquivoMidia = auxMusica;

                                            albumAux.lista.InserirNoFim(musica);
                                            auxAlbum = "";
                                        }

                                    }
                                }

                            }
                        } while (auxAlbum.IndexOf('|') != -1);
                        listaMusicasAlbum.Add(albumAux);
                    }
                    string[] readerCapaAlbum = File.ReadAllLines("dadosCapaAlbum.txt");
                    int countAuxLista = 0;
                    foreach (string l in readerCapaAlbum)
                    {
                        if (listaMusicasAlbum[countAuxLista] is AlbumMusical)
                            (listaMusicasAlbum[countAuxLista] as AlbumMusical).CapaAlbum = l;
                        countAuxLista++;
                    }
                }
                #endregion

                #region Carregando lista de músicas

                if (File.Exists("dadosMusica.txt"))
                {
                    string[] readerMusic = File.ReadAllLines("dadosMusica.txt");
                    if (readerMusic.Length > 1)
                    {
                        foreach (string l in readerMusic)
                        {
                            Musica musica = new Musica();
                            musica.Id = int.Parse(l.Substring(0, l.IndexOf('|')));
                            string auxMusica = l.Remove(0, l.IndexOf('|') + 1);
                            musica.Nome = auxMusica.Substring(0, auxMusica.IndexOf('|'));
                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                            musica.Descricao = auxMusica.Substring(0, auxMusica.IndexOf('|'));
                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                            musica.Volume = int.Parse(auxMusica.Substring(0, auxMusica.IndexOf('|')));
                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                            musica.Formato = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), auxMusica.Substring(0, auxMusica.IndexOf('|')));
                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                            musica.Duracao = double.Parse(auxMusica.Substring(0, auxMusica.IndexOf('|')));
                            auxMusica = auxMusica.Remove(0, auxMusica.IndexOf('|') + 1);
                            musica.ArquivoMidia = auxMusica;
                            listaMusicas.Add(musica);

                        }
                    }
                }
                #endregion


                #region Carregando nos pictureBox

                CarregandoPictureBox(listaMusicasAlbum, passo);

                #endregion
            }
            catch
            {

            }

        }

        private void lbProximo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (listaMusicas.Count > passo)
                passo += 6;
            LimparPictureBox();
            CarregandoPictureBox(listaMusicasAlbum, passo);
        }

        private void lbAnterior_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if ((passo - 6) >= 0)
                passo -= 6;
            LimparPictureBox();
            CarregandoPictureBox(listaMusicasAlbum, passo);
        }


        //Picture Box
        private void image4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            /*marcador = passo;
            if (marcador < listaMusicas.Count)
            {
                Reproduzir(listaMusicas[marcador].ArquivoMidia.ToString());
                //Reproduzir(lbId1.Content.ToString());
                PreencherBtnAlbum(image4, label9, label10);
            }*/

            for (int count = 0; count < listaMusicas.Count; count++)
            {
                if (lbId1.Content.ToString() == listaMusicas[count].ArquivoMidia)
                {
                    marcador = count;
                    break;
                }
            }
            PreencherBtnAlbum(image4, label9, label10);
            Reproduzir(listaMusicas[marcador].ArquivoMidia.ToString(), ref playing);

        }

        private void image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            for (int count = 0; count < listaMusicas.Count; count++)
            {
                if (lbId2.Content.ToString() == listaMusicas[count].ArquivoMidia)
                {
                    marcador = count;
                    break;
                }
            }
            PreencherBtnAlbum(image, label1, label2);
            Reproduzir(listaMusicas[marcador].ArquivoMidia.ToString(), ref playing);
        }

        private void image1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            for (int count = 0; count < listaMusicas.Count; count++)
            {
                if (lbId3.Content.ToString() == listaMusicas[count].ArquivoMidia)
                {
                    marcador = count;
                    break;
                }
            }
            PreencherBtnAlbum(image1, label3, label4);
            Reproduzir(listaMusicas[marcador].ArquivoMidia.ToString(), ref playing);
        }

        private void image2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            for (int count = 0; count < listaMusicas.Count; count++)
            {
                if (lbId4.Content.ToString() == listaMusicas[count].ArquivoMidia)
                {
                    marcador = count;
                    break;
                }
            }
            PreencherBtnAlbum(image2, label5, label5);
            Reproduzir(listaMusicas[marcador].ArquivoMidia.ToString(), ref playing);
        }

        private void image3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            for (int count = 0; count < listaMusicas.Count; count++)
            {
                if (lbId5.Content.ToString() == listaMusicas[count].ArquivoMidia)
                {
                    marcador = count;
                    break;
                }
            }
            PreencherBtnAlbum(image3, label7, label8);
            Reproduzir(listaMusicas[marcador].ArquivoMidia.ToString(), ref playing);
        }

        private void image5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            for (int count = 0; count < listaMusicas.Count; count++)
            {
                if (lbId6.Content.ToString() == listaMusicas[count].ArquivoMidia)
                {
                    marcador = count;
                    break;
                }
            }
            PreencherBtnAlbum(image5, label11, label12);
            Reproduzir(listaMusicas[marcador].ArquivoMidia.ToString(), ref playing);
        }

        private void bntPlay_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (playing == false)
            {
                playing = true;
                mePlayer.Play();
            }
            else
            {
                mePlayer.Pause();
                playing = false;
            }
        }

        private void btnForward_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (rdOrdemAdd.IsChecked == true)
            {
                if (marcador < listaMusicas.Count)
                {
                    marcador++;
                    Reproduzir(listaMusicas[marcador].ArquivoMidia, ref playing);
                }
            }
            if (rdLista.IsChecked == true)
            {
                if (listaC.Quantidade >= 1)
                {
                    var aux = listaC.RetornaPrimeiro();
                    listaC.RemoverDaPosicao(0);
                    Reproduzir((aux.Elemento as Musica).ArquivoMidia, ref playing);
                    lbNomeMusicaOuvindo.Content = (aux.Elemento as Musica).Nome;
                    lbNomeAlbum.Content = "";
                }


            }
            if (rdPilha.IsChecked == true)
            {
                if (pilhaC.Quantidade >= 1)
                {
                    var aux = pilhaC.Desempilhar();
                    Reproduzir((aux as Musica).ArquivoMidia, ref playing);
                    lbNomeMusicaOuvindo.Content = (aux as Musica).Nome;
                    lbNomeAlbum.Content = "";

                }
            }
            if(rdFila.IsChecked == true)
            {
                if (filaC.Quantidade >= 1)
                {
                    var aux = filaC.SairDaFila();
                    Reproduzir((aux as Musica).ArquivoMidia, ref playing);
                    lbNomeMusicaOuvindo.Content = (aux as Musica).Nome;

                }
            }
        }


        private void btnBackward_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            if (rdOrdemAdd.IsChecked == true)
            {
                if (marcador > 0)
                {
                    marcador--;
                    Reproduzir(listaMusicas[marcador].ArquivoMidia, ref playing);
                }
            }
           
        }

        private void lbOrdenaFotos_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            qPlaylist(ref q);
            gridFotos.Visibility = Visibility.Visible;
            gridConfigPlaylist.Visibility = Visibility.Hidden;
            AtualizarPlayList();
            gridVideoExibir.Visibility = Visibility.Hidden;
            gridMusicas2.Visibility = Visibility.Hidden;
            gridPrincipalMedia.Visibility = Visibility.Hidden;
            marcadorFoto = 0;
            if (File.Exists(@"..\..\foto\dadosFoto.txt"))
            {
                string[] fotos = File.ReadAllLines(@"..\..\foto\dadosFoto.txt");

                foreach (string l in fotos)
                {
                    Foto foto = new Foto();
                    string aux = l;
                    foto.Id = int.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    foto.Nome = aux.Substring(0, aux.IndexOf('|'));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    foto.Descricao = aux.Substring(0, aux.IndexOf('|'));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    foto.Local = aux.Substring(0, aux.IndexOf('|'));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    foto.MegaPixels = double.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    foto.TempoExibir = int.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    foto.AnoLancamento = int.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    foto.ArquivoMidia = aux;

                    listaFoto.Add(foto);
                }
                PreencherFoto();
            }

        }
        /// <summary>
        /// Métodos para a
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbOrdenaMusicas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            qPlaylist(ref q);
            gridFotos.Visibility = Visibility.Hidden;
            gridConfigPlaylist.Visibility = Visibility.Hidden;
            AtualizarPlayList();
            gridVideoExibir.Visibility = Visibility.Hidden;
            gridMusicas2.Visibility = Visibility.Visible;
            gridPrincipalMedia.Visibility = Visibility.Hidden;
        }

        private void lbOrdenaVideos_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void lbOrdenaVideos_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            qPlaylist(ref q);
            gridFotos.Visibility = Visibility.Hidden;
            gridPrincipalMedia.Visibility = Visibility.Hidden;
            gridMusicas2.Visibility = Visibility.Hidden;
            gridConfigPlaylist.Visibility = Visibility.Hidden;
            AtualizarPlayList();
            gridVideoExibir.Visibility = Visibility.Visible;
            lbVideoExibir.Items.Clear();

            listaVideos = new List<Video>();
            if (File.Exists(@"..\..\videos\dadosVideo.txt"))
            {
                string[] readerVideo = File.ReadAllLines(@"..\..\videos\dadosVideo.txt");
                foreach (string l in readerVideo)
                {
                    string aux = l;
                    Video video = new Video();

                    video.Id = int.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    video.Nome = aux.Substring(0, aux.IndexOf('|'));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    video.Descricao = aux.Substring(0, aux.IndexOf('|'));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    video.Idioma = (Enumeradores.EnumIdiomasVideo)Enum.Parse(typeof(Enumeradores.EnumIdiomasVideo), aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    video.PossuiLegenda = bool.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    video.Formato = (Enumeradores.FormatoEnumVideo)Enum.Parse(typeof(Enumeradores.FormatoEnumVideo), aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    video.AnoLancamento = int.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    video.ArquivoMidia = aux;

                    listaVideos.Add(video);
                }

                foreach (var l in listaVideos)
                {
                    lbVideoExibir.Items.Add(l.Nome);
                }


            }
        }

        private void lbAssistirVideo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AtualizarPlayList();
            gridVideoExibir.Visibility = Visibility.Hidden;
            gridPrincipalMedia.Visibility = Visibility.Visible;

            playing = true;
            try
            {
                Reproduzir(listaVideos[lbVideoExibir.SelectedIndex].ArquivoMidia, ref playing);
                lbNomeVideo.Content = listaVideos[lbVideoExibir.SelectedIndex].Nome.ToString();
                lbNomeMusicaOuvindo.Content = listaVideos[lbVideoExibir.SelectedIndex].Nome.ToString();
                lbNomeAlbum.Content = listaVideos[lbVideoExibir.SelectedIndex].AnoLancamento.ToString();
                btnAlbum.Source = ImagemUri(@"..\..\videos\video.png");
            }
            catch { }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            bool apoio = true;
            try
            {
                if (lbMusica.SelectedItem.ToString() != "")
                {
                    for (int i = 0; i < lbMusicaApoio.Items.Count; i++)
                    {


                        string nome = lbMusicaApoio.Items[i].ToString().ToUpper().Trim();
                        string queCaralho = lbMusica.SelectedItem.ToString().ToUpper().Trim();
                        if (queCaralho == nome)
                            apoio = false;

                    }
                    if (apoio == true)
                        lbMusicaApoio.Items.Add(lbMusica.SelectedItem);
                }
                else
                    throw new Exception("Escolha uma musica para adicionar");
            }
            catch
            {
                MessageBox.Show("Escolha uma musica para adicionar");
            }
            return;
        }

        private void btnRemoveMusicaPlaylist_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lbMusicaApoio.Items.Count != 0)
                {

                    if (lbMusicaApoio.SelectedItem.ToString().Length != 0)
                    {
                        string removeKrai = lbMusicaApoio.SelectedItem.ToString();
                        lbMusicaApoio.Items.Remove(removeKrai);

                    }
                    else
                    {
                        MessageBox.Show("Escolha uma musica para remover");
                    }
                }
            }
            catch
            {
                MessageBox.Show("Escolha um item para remover");
            }
            return;
        }

        private void btnCriar_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                if (txtNomeDaPlayList.Text == "")
                    throw new Exception("Digite o nome da PlayList");
                if (lbMusicaApoio.Items.Count == 0)
                    throw new Exception("Necessita de ao menos uma musica");


                List<Musica> criandoPlayList = new List<Musica>();
                for (int i = 0; i < lbMusicaApoio.Items.Count; i++)
                {
                    for (int x = 0; x < help.Musicas.Count; x++)
                    {

                        string nome = lbMusicaApoio.Items[i].ToString().ToUpper().Trim();
                        string seiLa = help.Musicas[x].Nome.ToUpper().Trim();
                        if (nome == seiLa)
                        {
                            criandoPlayList.Add(help.Musicas[x]);


                        }

                    }
                }
                int z = lbMusicaApoio.Items.Count;
                for (int w = 0; w < z; w++)
                    lbMusicaApoio.Items.Remove(lbMusicaApoio.Items[0]);
                if (test == true)
                {

                    help.CriarPlayList(txtNomeDaPlayList.Text, criandoPlayList);
                    RemoverListBox(lbMusicaApoio);
                    ckbAlterarPlayList.IsChecked = false;
                    RemoverItemsComboBox(cbPlayList);
                    MessageBox.Show("PlayList criada com sucesso");
                    txtNomeDaPlayList.Text = "";
                    cbPlayList.Visibility = Visibility.Hidden;
                    btnAlterar.Visibility = Visibility.Hidden;
                    btnRemovePlayList.Visibility = Visibility.Hidden;
                }
                else
                {
                    ckbAlterarPlayList.IsChecked = false;

                    RemoverListBox(lbMusicaApoio);
                    help.AlterarPlayList(cbPlayList.SelectedItem.ToString(), criandoPlayList);
                    RemoverItemsComboBox(cbPlayList);
                    cbPlayList.Visibility = Visibility.Hidden;
                    btnAlterar.Visibility = Visibility.Hidden;
                    btnRemovePlayList.Visibility = Visibility.Hidden;

                    test = true;
                    txtNomeDaPlayList.Text = "";
                    controleAlterar = true;
                    MessageBox.Show("Alterada com sucesso");
                }
            }
            catch (Exception erro)
            {
                if (erro.Message == "Ja existe uma play list com esse nome")
                    txtNomeDaPlayList.Text = "";
                MessageBox.Show(erro.Message);

            }
            return;
        }

        private void ckbAlterarPlayList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ckbAlterarPlayList.IsChecked == true)
                {
                    int verifica = 0;
                    if (File.Exists("nomeDasPlayList.txt"))
                    {
                        string[] verificaSeTemPlayList = File.ReadAllLines("nomeDasPlayList.txt");
                        verifica = verificaSeTemPlayList.Length;
                    }
                    if (!File.Exists("nomeDasPlayList.txt") || verifica == 0)
                    {
                        ckbAlterarPlayList.IsChecked = false;
                        throw new Exception("Não existe nenhuma play list");
                    }
                    string[] nomePlayList = File.ReadAllLines("nomeDasPlayList.txt");
                    if (nomePlayList.Length != 0)
                    {
                        for (int x = 0; x < nomePlayList.Length; x++)
                        {

                            cbPlayList.Items.Add(nomePlayList[x]);
                        }


                    }
                    cbPlayList.Visibility = Visibility.Visible;
                    btnAlterar.Visibility = Visibility.Visible;
                    btnRemovePlayList.Visibility = Visibility.Visible;
                }
                else
                {
                    cbPlayList.Visibility = Visibility.Hidden;
                    btnAlterar.Visibility = Visibility.Hidden;
                    btnRemovePlayList.Visibility = Visibility.Hidden;
                    RemoverItemsComboBox(cbPlayList);
                }
            }
            catch (Exception erro)
            {

                MessageBox.Show(erro.Message);
            }
            return;
        }

        private void btnAlterar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (controleAlterar == true)
                {
                    if (cbPlayList.SelectedItem.ToString().Length != 0)
                    {

                        RemoverListBox(lbMusicaApoio);
                        Musica apoio;
                        txtNomeDaPlayList.Text = cbPlayList.SelectedItem.ToString();
                        if (!File.Exists($"{cbPlayList.SelectedItem.ToString() }.txt"))
                            File.Create($"{cbPlayList.SelectedItem.ToString()}.txt");
                        string[] quebrandoLinhas = File.ReadAllLines($"{cbPlayList.SelectedItem.ToString()}.txt");
                        if (quebrandoLinhas.Length != 0)
                        {


                            for (int i = 0; i < quebrandoLinhas.Length; i++)
                            {
                                apoio = new Musica();
                                string[] quebrandoPipe = quebrandoLinhas[i].Split('|');
                                apoio.Id = Convert.ToInt32(quebrandoPipe[0]);
                                apoio.Nome = quebrandoPipe[1];
                                apoio.Descricao = quebrandoPipe[2];
                                apoio.Volume = Convert.ToInt32(quebrandoPipe[3]);
                                Enumeradores.FormatoEnumMusica form = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), quebrandoPipe[4].ToString());
                                apoio.Formato = form;
                                apoio.Duracao = double.Parse(quebrandoPipe[5]);
                                apoio.ArquivoMidia = quebrandoPipe[6];
                                lbMusicaApoio.Items.Add(apoio.Nome);


                            }
                            test = false;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Selecione um item");
            }
            return;
        }

        private void btnRemovePlayList_Click(object sender, RoutedEventArgs e)
        {
            // MessageBox.Show("Tem certeza que deseja remover a playlist?","Notificação", MessageBoxButton.OKCancel,MessageBoxImage.Question,MessageBoxResult.OK);
            /* if(MessageBoxResult.OK.ToString()=="1")
             {
                 help.RemoverPlayList(cbPlayList.SelectedItem.ToString());
                 ckbAlterarPlayList.IsChecked = false;
                 btnRemoveMusicaPlaylist.Visibility = Visibility.Hidden;
                 btnAlterar.Visibility = Visibility.Hidden;
                 cbPlayList.Visibility = Visibility.Hidden;
                 RemoverListBox(lbMusicaApoio);
                 RemoverItemsComboBox(cbPlayList);
             }
             */
            MessageBox.Show("PlayList removida com sucesso");
            help.RemoverPlayList(cbPlayList.SelectedItem.ToString());
            ckbAlterarPlayList.IsChecked = false;
            txtNomeDaPlayList.Text = "";
            btnRemovePlayList.Visibility = Visibility.Hidden;
            btnAlterar.Visibility = Visibility.Hidden;
            cbPlayList.Visibility = Visibility.Hidden;
            RemoverListBox(lbMusicaApoio);
            RemoverItemsComboBox(cbPlayList);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            gridAlbum.Visibility = Visibility.Visible;
            gridFotos.Visibility = Visibility.Visible;
            gridMusicas2.Visibility = Visibility.Visible;
            q = 0;
            gridConfigPlaylist.Visibility = Visibility.Hidden;
            txtNomeDaPlayList.Text = "";
            RemoverListBox(lbMusicaApoio);
            RemoverListBox(lbMusica);
            RemoverItemsComboBox(cbPlayList);
            ckbAlterarPlayList.IsChecked = false;
        }

        private void lsBarraLateral_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lsBarraLateral.SelectedIndex != -1)
            {
                marcadorPlaylist = 0;
                pilhaC = new Pilha();
                filaC = new Fila();
                listaC = new Lista();
                musicaPlaylist = new List<Musica>();
                string[] readerPlaylist = File.ReadAllLines(lsBarraLateral.SelectedItem + ".txt");
                foreach (string l in readerPlaylist)
                {
                    string aux = l;
                    Musica musica = new Musica();
                    musica.Id = int.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    musica.Nome = aux.Substring(0, aux.IndexOf('|'));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    musica.Descricao = aux.Substring(0, aux.IndexOf('|'));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    musica.Volume = int.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    musica.Formato = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    musica.Duracao = double.Parse(aux.Substring(0, aux.IndexOf('|')));
                    aux = aux.Remove(0, aux.IndexOf('|') + 1);
                    musica.ArquivoMidia = aux;

                    musicaPlaylist.Add(musica);
                }
                //Reproduzir(musicaPlaylist[marcadorPlaylist].ArquivoMidia,ref playing);
                // Colocar o album, e ajeitar as formas como usamos as setas -> Boleanos
                if (rdLista.IsChecked == true)
                {
                    marcadorListaC = 0;
                    CarregarLista();
                    var aux = listaC.RetornaPrimeiro();
                    listaC.RemoverDaPosicao(0);
                    Reproduzir((aux.Elemento as Musica).ArquivoMidia, ref playing);
                    lbNomeMusicaOuvindo.Content = (aux.Elemento as Musica).Nome;
                    btnAlbum.Source = ImagemUri("lista.png");
                    lbNomeAlbum.Content = "";



                }
                else if (rdPilha.IsChecked == true)
                {
                    marcadorP = 0;
                    CarregarPilha();
                    var aux = pilhaC.Desempilhar();
                    Reproduzir((aux as Musica).ArquivoMidia, ref playing);
                    lbNomeMusicaOuvindo.Content = (aux as Musica).Nome;
                    btnAlbum.Source = ImagemUri("Pilha.png");
                    lbNomeAlbum.Content = "";
                }
                else if (rdFila.IsChecked == true)
                {
                    marcadorF = 0;
                    CarregarFila();
                    var aux = filaC.SairDaFila();
                    Reproduzir((aux as Musica).ArquivoMidia, ref playing);
                    lbNomeMusicaOuvindo.Content = (aux as Musica).Nome;
                    btnAlbum.Source = ImagemUri("fila.png");
                    lbNomeAlbum.Content = "";
                }
            }
        }

        private void label14_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Sobre sobre = new Sobre();
            sobre.ShowDialog();
        }
    }
}
