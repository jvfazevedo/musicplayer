﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Enumeradores
{
    public enum FormatoEnumVideo
    {
        AVI,
        WMV,
        MKV,
        MP4,
        MPEG
    }
}
