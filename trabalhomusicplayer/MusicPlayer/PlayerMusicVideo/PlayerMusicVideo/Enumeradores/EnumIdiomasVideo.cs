﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo.Enumeradores
{
    public enum EnumIdiomasVideo
    {
        PORTUGUÊS,
        INGLÊS,
        ESPANHOL,
        OUTROS
    }
}
