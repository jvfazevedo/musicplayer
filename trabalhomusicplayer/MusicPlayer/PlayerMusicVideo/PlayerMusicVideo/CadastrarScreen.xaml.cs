﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using PlayerMusicVideo.Classes;
using NAudio.Wave;

namespace PlayerMusicVideo
{
    /// <summary>
    /// Interaction logic for CadastrarScreen.xaml
    /// </summary>
    public partial class CadastrarScreen : Window

    {
        //Variaveis 
        public OpenFileDialog CapaAlbum;
        public string[] valores;
        public string[] idMusicas;
        public string line;
        public string lineAux;
        public string auxLine;
        public bool confirmar = false;
        AlbumMusical albumPrincipal = new AlbumMusical();
        /// <summary>
        /// Método para fazer a classificacao
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public static int Classificacao(string arquivo)
        {
            if (arquivo == ".wav" || arquivo == ".mp3" || arquivo == ".wma")
                return 0;
            else if (arquivo == ".mp4" || arquivo == ".avi" || arquivo == ".wmv" || arquivo == ".mkv" || arquivo == ".mpe")
                return 1;
            else if (arquivo == ".jpg" || arquivo == ".png" || arquivo == ".jpe")
                return 2;
            else
                return -1;

        }
        /// <summary>
        /// Métodos para resetar os campos
        /// </summary>
        public void ResetarCampos()
        {
            gridMusicas.Visibility = Visibility.Hidden;
            gridVideos.Visibility = Visibility.Hidden;
            gridFoto.Visibility = Visibility.Hidden;
            lbAlbum.Visibility = Visibility.Hidden;
            txtAlbum.Visibility = Visibility.Hidden;
            btnSalvar.Visibility = Visibility.Hidden;
            cbFormato.Items.Clear();
            cbFormatoMusica.Items.Clear();
            lbCapaDoAlbum.Content= "";
            CapaAlbum = new OpenFileDialog();
            txtAno.Text = "";
            btnCarregarCapaDoAlbum.Visibility = Visibility.Hidden;
            txtNome.Text = "";
            txtAnoLancamento.Text = "";
            txtArtista.Text = "";
            cbIdioma.Items.Clear();
            txtDescricao.Text = "";
            txtDuracao.Text = "";
            txtLocal.Text = "";
            txtId.Text = "";
            txtResolucao.Text = "";
            txtAlbum.Text = "";
            txtLista.Text = "";
            txtTempo.Text = "";
            txtVolume.Text = "";
        }
        public CadastrarScreen()
        {
            InitializeComponent();
        }
        
        public OpenFileDialog procurarArquivo;
        public double tempoTxt = 0;
        /// <summary>
        /// Botao clique 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCadastrar_Click(object sender, RoutedEventArgs e)
        {
            //Resetar Campos
            ResetarCampos();

            btnSalvar.Visibility = Visibility.Visible;

            procurarArquivo = new OpenFileDialog();
            
            procurarArquivo.Filter = "Músicas (.mp3;.wav;.wma)|*.mp3;*wav;*wma|Videos (.mp4;.avi;.wmv;,mkv;mpe)|*.mp4;*.avi;*.wmv;*.mkv;*.mpe|Fotos (.jpg, .png,.jpeg)|*.jpg;*png;*jpeg";
            if(procurarArquivo.ShowDialog() == true)
            {
                double segundo;
                txtNome.Text = procurarArquivo.SafeFileName.ToString();
                double Duracao;
                AudioFileReader audio = new AudioFileReader(procurarArquivo.FileName);
                segundo = audio.TotalTime.TotalSeconds;
                tempoTxt = segundo / 60;
                string tempoTxtString = String.Format("{0:N2}", tempoTxt);
               // Duracao = Math.Truncate(Duracao);
                //segundo = segundo - Duracao * 60;
                //txtDuracao.Text = Convert.ToString(Duracao) + ":" + Convert.ToString(segundo);
                
                audio.Close();
                txtDuracao.Text = tempoTxtString;

                if (Classificacao(procurarArquivo.SafeFileName.Substring(procurarArquivo.SafeFileName.IndexOf('.'), 4)) == 0)
                {
                    gridMusicas.Visibility = Visibility.Visible;
                    lbAlbum.Visibility = Visibility.Visible;
                    txtAlbum.Visibility = Visibility.Visible;

                    foreach (string l in Enum.GetNames(typeof(Enumeradores.FormatoEnumMusica)))
                        cbFormatoMusica.Items.Add(l.ToString());

                    string aux = procurarArquivo.SafeFileName.Remove(0, procurarArquivo.SafeFileName.IndexOf('.') + 1);
                    aux = aux.ToUpper();
                    int count = 0;

                    foreach (var l in cbFormatoMusica.Items)
                    {
                        if (aux == l.ToString())
                            cbFormatoMusica.SelectedIndex = count;
                        count++;

                    }
                }
                else if (Classificacao(procurarArquivo.SafeFileName.Substring(procurarArquivo.SafeFileName.IndexOf('.'), 4)) == 1)
                {
                    gridVideos.Visibility = Visibility.Visible;

                    foreach(string l in Enum.GetNames(typeof(Enumeradores.FormatoEnumVideo)))
                        cbFormato.Items.Add(l);
                    foreach (string l in Enum.GetNames(typeof(Enumeradores.EnumIdiomasVideo)))
                        cbIdioma.Items.Add(l);

                    string aux = procurarArquivo.SafeFileName.Remove(0, procurarArquivo.SafeFileName.IndexOf('.') + 1);
                    aux = aux.ToUpper();
                    int count = 0;

                    foreach(var l in cbFormato.Items)
                    {
                        if (aux == l.ToString())
                            cbFormato.SelectedIndex = count;
                        count++;
                            
                    }
                }
                else if (Classificacao(procurarArquivo.SafeFileName.Substring(procurarArquivo.SafeFileName.IndexOf('.'), 4)) == 2)
                    gridFoto.Visibility = Visibility.Visible;
            }
            
        }
        /// <summary>
        /// evento botão salvar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSalvar_Click(object sender, RoutedEventArgs e)
        {
            #region gridMusicas is Visible
            if (gridMusicas.Visibility == Visibility.Visible)
            {
                //Toda música já é salva tendo seu album. Assim uma musica so é criada junto com o album.
                Musica musica = new Musica();
                AlbumMusical album = new AlbumMusical();

                //Preenchendo com as informações da tela de cadastro
                musica.Nome = txtNome.Text;
                musica.Id = int.Parse(txtId.Text);

                album.Id = int.Parse(txtId.Text);
                album.Nome = txtAlbum.Text;

                musica.Descricao = txtDescricao.Text;

                if (!string.IsNullOrEmpty(CapaAlbum.SafeFileName))
                {
                    File.Copy(CapaAlbum.FileName, AppDomain.CurrentDomain.BaseDirectory.ToString() + @"..\..\albuns\" + CapaAlbum.SafeFileName, true);
                    album.CapaAlbum = @"..\..\albuns\" + CapaAlbum.SafeFileName;
                }
                album.Artista = txtArtista.Text;

                musica.Duracao = double.Parse(txtDuracao.Text);
                musica.Volume = int.Parse(txtVolume.Text);
                Enumeradores.FormatoEnumMusica form = (Enumeradores.FormatoEnumMusica)Enum.Parse(typeof(Enumeradores.FormatoEnumMusica), cbFormatoMusica.SelectedItem.ToString());
                musica.Formato = form;

                album.AnoLancamento = int.Parse(txtAnoLancamento.Text);

                //Verificando diretório
                if (!File.Exists(@"..\..\musicas"))
                    Directory.CreateDirectory(@"..\..\musicas");
                if (!!File.Exists(@"..\..\album"))
                    Directory.CreateDirectory(@"..\..\album");

                if (confirmar == true)
                {
                    album = albumPrincipal;
                    album.Nome = txtAlbum.Text;
                    album.lista.InserirNoFim(musica.Id.ToString());
                    album.Alterar(album,auxLine);
                }

                else {
                    album.lista.InserirNoFim(musica.Id.ToString());
                    album.Incluir(album);
                }
                File.Copy(procurarArquivo.FileName, AppDomain.CurrentDomain.BaseDirectory.ToString() + @"..\..\musicas\"+procurarArquivo.SafeFileName,true);
                musica.ArquivoMidia = @"..\..\musicas\" + musica.Nome;
                musica.Incluir(musica);
                //////////////////////////////////////////////////////////////////////////
                
            }
            #endregion

            #region gridVideo is Visible
            if (gridVideos.Visibility == Visibility.Visible)
            {
                Video video = new Video();
                video.Nome = procurarArquivo.SafeFileName;
                video.Id = int.Parse(txtId.Text);
                //Fazendo cast para enumeradores

                Enumeradores.FormatoEnumVideo form = (Enumeradores.FormatoEnumVideo)Enum.Parse(typeof(Enumeradores.FormatoEnumVideo), cbFormato.SelectedItem.ToString());
                video.Formato = form;
                Enumeradores.EnumIdiomasVideo idiom = (Enumeradores.EnumIdiomasVideo)Enum.Parse(typeof(Enumeradores.EnumIdiomasVideo), cbIdioma.SelectedItem.ToString());
                video.Idioma = idiom;
                video.Descricao = txtDescricao.Text;
                
                if (rdTrue.IsChecked == false)
                    video.PossuiLegenda = false;
                else
                    video.PossuiLegenda = true;

                //Verificando diretório para copiar arquivo
                if (!Directory.Exists(@"..\..\videos"))
                    Directory.CreateDirectory(@"..\..\videos");
                File.Copy(procurarArquivo.FileName, AppDomain.CurrentDomain.BaseDirectory.ToString() + @"..\..\videos\" + procurarArquivo.SafeFileName,true);

                video.ArquivoMidia =@"..\..\videos\" + procurarArquivo.SafeFileName;
                video.AnoLancamento = int.Parse(txtAno.Text.ToString());

                video.Incluir(video);

            }
            #endregion

            #region gridFotos is Visible
            if (gridFoto.Visibility == Visibility.Visible)
            {
                Foto foto = new Foto();
                foto.Nome = procurarArquivo.SafeFileName;
                foto.Id = int.Parse(txtId.Text);
                foto.Local = txtLocal.Text;
                foto.MegaPixels = double.Parse(txtResolucao.Text);
                foto.TempoExibir = int.Parse(txtTempo.Text);
                foto.Descricao = txtDescricao.Text;
                foto.AnoLancamento = int.Parse(txtAnoFoto.Text);

                if (!Directory.Exists(@"..\..\foto"))
                    Directory.CreateDirectory(@"..\..\foto");
                File.Copy(procurarArquivo.FileName, AppDomain.CurrentDomain.BaseDirectory.ToString() + @"..\..\foto\" + procurarArquivo.SafeFileName,true);
                foto.ArquivoMidia = @"..\..\foto\" + procurarArquivo.SafeFileName;
                foto.Incluir(foto);

            }
            #endregion
            MessageBox.Show("Adicionado com sucesso");
            ResetarCampos();
        }

        private void txtAlbum_LostFocus(object sender, RoutedEventArgs e)
        {
            #region Verificação, preenchimento da lista de músicas, Recolocação do album para alteração
            confirmar = false;
            albumPrincipal = null;
            albumPrincipal = new AlbumMusical();
            txtLista.Text = "";
            btnCarregarCapaDoAlbum.Visibility = Visibility.Hidden;
            
            //Carregamento do Album
            if (File.Exists("dadosAlbum.txt"))
            {
                
                StreamReader file = new StreamReader("dadosAlbum.txt");
                while ((line = file.ReadLine()) != null)
                {
                    
                    if (line.Substring(0, line.IndexOf('|')) == txtAlbum.Text.Trim())
                    {
                        valores = line.Split('|');
                        albumPrincipal.Nome = valores[0];
                        albumPrincipal.Id = int.Parse(valores[1]);
                        albumPrincipal.Artista = valores[2];
                        albumPrincipal.AnoLancamento = int.Parse(valores[3]);
                        auxLine = line;
                        

                        for(int i = 4; i < valores.Length; i++)
                        {
                            albumPrincipal.lista.InserirNoFim(valores[i]);
                            
                        }
                        confirmar = true;

                        #region Adicionar na lista de músicas o nome das músicas
                        txtLista.Text = line;
                        string aux = "";
                        idMusicas = line.Split('|');
                        for(int i = 4; i < idMusicas.Length; i++)
                        {
                            StreamReader fileAux = new StreamReader("dadosMusica.txt");
                            while ((lineAux = fileAux.ReadLine()) != null)
                            {
                                if (idMusicas[i] == lineAux.Substring(0, lineAux.IndexOf('|')))
                                {
                                    lineAux = lineAux.Remove(0, lineAux.IndexOf('|') + 1);
                                    aux += lineAux.Substring(0, lineAux.IndexOf('|')) + Environment.NewLine;
                                    break;
                                }
                            }
                            fileAux.Close();

                        }
                        txtLista.Text = aux;
                        
                        #endregion
                    }
                }
                file.Close();
            }

            #endregion
            if (string.IsNullOrEmpty(txtLista.Text))
            {
                btnCarregarCapaDoAlbum.Visibility = Visibility.Visible;
            }
        }

        private void btnCarregarCapaDoAlbum_Click(object sender, RoutedEventArgs e)
        {
            CapaAlbum = new OpenFileDialog();
            CapaAlbum.Filter = "Fotos (.jpg;.jpeg;.png)|*.jpg;*jpeg;*png;";
            if(CapaAlbum.ShowDialog() ==true)
            {
                lbCapaDoAlbum.Content = CapaAlbum.SafeFileName;
                
            }
        }
    }
}
