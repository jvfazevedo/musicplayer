﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    public class NodoFila
    {
        private NodoFila proximo;
        private Object elemento;

        public NodoFila Proximo
        {
            get
            {
                return proximo;
            }
            set
            {
                proximo = value;
            }
        }
        public Object Elemento
        {
            get { return elemento; }
            set { elemento = value; }
        }
    }
}
