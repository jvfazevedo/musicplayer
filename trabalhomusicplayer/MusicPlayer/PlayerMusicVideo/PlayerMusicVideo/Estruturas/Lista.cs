﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    public class Lista 
    {
        NodoLista primeiro = null;
        NodoLista ultimo = null;
        int qtde = 0;
        
        public int Quantidade
        {
            get { return qtde; }
        }
        private void InserirNaPosicao(NodoLista anterior, Object valor)
        {
            NodoLista novo = new NodoLista();
            novo.Elemento = valor;

            if (anterior == null)
            {
                if (qtde == 0)
                    primeiro = novo;
                else
                {
                    novo.Proximo = primeiro;
                    primeiro.Anterior = novo;
                    primeiro = novo;
                }
            }
            else
            {
                novo.Proximo = anterior.Proximo;
                anterior.Proximo = novo;
                novo.Anterior = anterior;
                if (novo.Proximo != null)
                {
                    novo.Proximo.Anterior = novo;
                }
            }

            if (novo.Proximo == null)
                ultimo = novo;

            qtde++;
        }

        public void InserirNoInicio(Object valor)
        {
            InserirNaPosicao(null, valor);
        }
        public void InserirNoFim(Object valor)
        {
            if (qtde == 0)
            {
                InserirNoInicio(valor);
            }
            else
            {
                InserirNaPosicao(ultimo, valor);
            }
        }
        /// <summary>
        /// Insere em uma posição, iniciando do 0
        /// </summary>
        /// <param name="valor">valor</param>
        /// <param name="posicao">posicao iniciando do 0</param>
        public void InserirNaPosicao(Object valor, int posicao)
        {
            if (posicao > qtde || posicao < 0)
                throw new Exception("Não é possível inserir.");

            if (posicao == 0)
                InserirNoInicio(valor);
            else
            {
                NodoLista aux = primeiro;
                for (int i = 1; i < posicao; i++)
                    aux = aux.Proximo;

                InserirNaPosicao(aux, valor);
            }
        }
        public Object RemoverDaPosicao(int posicao)
        {
            if (posicao >= qtde || posicao < 0 || qtde == 0)
                throw new Exception("Não é possível remover.");

            Object valor = new Object();
            qtde--;

            if (qtde == 0)
            {
                valor = primeiro.Elemento;
                primeiro = ultimo = null;
                return valor;
            }
            else
            {
                //nodoApagado irá armazenar o nodo será apagado.
                NodoLista nodoApagado = primeiro;
                for (int i = 1; i <= posicao; i++)  // encontra o elemento anterior ao que será apagado
                    nodoApagado = nodoApagado.Proximo;

                valor = nodoApagado.Elemento;

                if (nodoApagado.Proximo == null) // ajusta o último
                    ultimo = nodoApagado.Anterior;

                if (nodoApagado.Anterior == null) // ajusta o primeiro
                    primeiro = nodoApagado.Proximo;

                if (nodoApagado.Anterior != null)
                    nodoApagado.Anterior.Proximo = nodoApagado.Proximo;

                if (nodoApagado.Proximo != null)
                    nodoApagado.Proximo.Anterior = nodoApagado.Anterior;

                return valor;
            }
        }
        public bool Pesquisa(Object dado)
        {
            NodoLista aux = primeiro;
            while (aux != null)
            {
                if (dado is Classes.Musica)
                {
                    if ((aux.Elemento as Classes.Musica).Id == (dado as Classes.Musica).Id)
                        return true;
                    
                }
                aux = aux.Proximo;
            }
            return false;
        }
        public Object ListarIterativo()
        {
            Object r = string.Empty;

            NodoLista aux = primeiro;
            while (aux != null)
            {
                r = r + Environment.NewLine + aux.Elemento;
                aux = aux.Proximo;
            }
            return (r as string).Trim();
        }
        /// <summary>
        /// Método listar recursivo
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        private Object Listar(NodoLista e)
        {
            if (e != null)
                return Listar(e.Proximo) + "|" + e.Elemento;
            else
                return string.Empty;
        }


        public Object ListarRecursivo()
        {
            return Listar(RetornaPrimeiro());
        }


        public NodoLista RetornaPrimeiro()
        {
            return primeiro;
        }
    }
}
