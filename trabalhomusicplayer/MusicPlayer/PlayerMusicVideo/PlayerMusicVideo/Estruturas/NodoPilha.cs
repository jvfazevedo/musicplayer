﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    public class NodoPilha
    {
        private Object elemento;
        private NodoPilha anterior;

        public NodoPilha Anterior
        {
            get { return anterior; }
            set { anterior = value; }
        }

        public Object Elemento
        {
            get
            {
                return elemento;
            }
            set
            {
                if (value is Object)
                {
                    this.elemento = value;
                }
            }
        }
    }
}
