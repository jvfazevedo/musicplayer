﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    public class NodoLista
    {
        private NodoLista proximo;
        private NodoLista anterior;
        private Object elemento;

        public Object Elemento
        {
            get { return elemento; }
            set { elemento = value; }
        }

        public NodoLista Proximo
        {
            get { return proximo; }
            set { proximo = value; }
        }

        public NodoLista Anterior
        {
            get { return anterior; }
            set { anterior = value; }
        }       
    }
}
