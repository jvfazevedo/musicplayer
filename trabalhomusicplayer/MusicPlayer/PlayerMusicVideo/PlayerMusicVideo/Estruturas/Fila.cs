﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    public class Fila
    {
        private NodoFila primeiro = null;
        private NodoFila ultimo = null;
        private int quantidade = 0;


        public int Quantidade { get { return quantidade; } }
        public void AdicionarNaFila(Object elemento)
        {
            NodoFila nodoAux = new NodoFila();
            nodoAux.Elemento = elemento;
            nodoAux.Proximo = null;

            if (quantidade == 0)
                primeiro = nodoAux;
            else
                ultimo.Proximo = nodoAux;
            ultimo = nodoAux;
            quantidade++;
        }
        public Object SairDaFila()
        {
            if (Quantidade == 0)
                throw new ErroFila("Não há elementos na fila");
            else
            {
                Object elementoAux = new Object();
                elementoAux = primeiro.Elemento;
                primeiro = primeiro.Proximo;
                quantidade--;
                if (quantidade == 0)
                    ultimo = null;
                return elementoAux;
            }
        }
        public Object RetornaTopoFila()
        {
            if (quantidade != 0)
                return primeiro.Elemento;
            else
                throw new ErroFila("Não há topo");
        }
    }
}
