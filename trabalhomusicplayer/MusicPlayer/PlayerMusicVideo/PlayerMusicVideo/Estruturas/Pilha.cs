﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    public class Pilha
    {
        private NodoPilha topo = null;
        private int quantidade = 0;

        public int Quantidade
        {
            get { return quantidade; }
        }
        public void Empilhar(Object elemento)
        {
            NodoPilha nodoAux = new NodoPilha();
            nodoAux.Elemento = elemento;
            nodoAux.Anterior = topo;
            topo = nodoAux;
            quantidade++;
        }
        public Object Desempilhar()
        {
            NodoPilha nodoaux = new NodoPilha();
            if (topo != null)
            {
                nodoaux = topo;
                topo = topo.Anterior;
                quantidade--;
                return nodoaux.Elemento;
            }
            else
            {
                throw new ErroDesempilha("Pilha vazia");
            }
        }
        public Object RetornaTopo()
        {
            if (topo == null)
                throw new ErroRetornaTopo("Pilha Vazia");
            else
                return topo.Elemento;
        }
    }
}
