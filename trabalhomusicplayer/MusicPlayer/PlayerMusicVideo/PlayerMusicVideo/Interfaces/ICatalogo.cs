﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    interface ICatalogo // Dentro de propriedades você só pode estabelecer propriedades e métodos
                        // Nunca poderá ser privado.
    {
        int AnoLancamento { get; set; }
    }
}
