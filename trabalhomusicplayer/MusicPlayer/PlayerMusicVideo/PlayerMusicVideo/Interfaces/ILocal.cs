﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicVideo
{
    public interface ILocal
    {
        string ArquivoMidia { get; set; }
        bool ValidaCaminho(string caminho);
    }
}
